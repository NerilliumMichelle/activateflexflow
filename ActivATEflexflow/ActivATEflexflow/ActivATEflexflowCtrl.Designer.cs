namespace ActivATE.ActivATEflexflow
{
    partial class ActivATEflexflowCtrl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActivATEflexflowCtrl));
            this.buttonDoWork = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonDoWork
            // 
            this.buttonDoWork.Location = new System.Drawing.Point(12, 12);
            this.buttonDoWork.Name = "buttonDoWork";
            this.buttonDoWork.Size = new System.Drawing.Size(92, 38);
            this.buttonDoWork.TabIndex = 0;
            this.buttonDoWork.Text = "Do Work";
            this.buttonDoWork.UseVisualStyleBackColor = true;
            this.buttonDoWork.Click += new System.EventHandler(this.buttonDoWork_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.BackColor = System.Drawing.SystemColors.Info;
            this.labelStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelStatus.Location = new System.Drawing.Point(0, 277);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(474, 39);
            this.labelStatus.TabIndex = 2;
            this.labelStatus.Text = "Status";
            // 
            // ActivATEflexflowCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 316);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.buttonDoWork);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ActivATEflexflowCtrl";
            this.Text = "ActivATEflexflowCtrl";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonDoWork;
        private System.Windows.Forms.Label labelStatus;
    }
}