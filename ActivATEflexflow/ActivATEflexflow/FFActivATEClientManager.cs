﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActivATE.ActivATEflexflow
{
    public sealed class FFActivATEClientManager
    {
        private static volatile FFActivATEClientManager instance;
        private static object syncRoot = new Object();

        private FFActivATEClientManager() { }

        public static FFActivATEClientManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new FFActivATEClientManager();
                    }
                }

                return instance;
            }
        }
        public String ServiceURL { get;  set; }
        public String UserID { get;  set; }
        public String SerialInfo { get;  set; }
        public String StationName { get;  set; }
    }
}
