//=====================================================================================
// <copyright file="ActivATEflexflowCtrl.cs" company="EADS North America">   
//     Copyright 2008, EADS North America Test And Services. All rights reserved.   
// </copyright>
//=====================================================================================
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// 
// THIS SOFTWARE MAY ONLY BE USED SUBJECT TO AN EXECUTED SOFTWARE LICENSE AGREEMENT 
// BETWEEN THE USER AND EADS.  YOU HAVE NO RIGHT TO USE OR EXPLOIT THIS MATERIAL 
// EXCEPT SUBJECT TO THE TERMS OF SUCH AN AGREEMENT.
//=====================================================================================

/**************************************************************************************
* Assembly Name:        ActivATEflexflow.dll
* 
* File Name:            ActivATEflexflowCtrl.cs
* 
* Purpose:
* 
* Revision History:
* 
* Dev Notes:
* 
**************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Racal.Interfaces;

namespace ActivATE.ActivATEflexflow
{
    /// <summary>
    /// Provides the user interface for the ActivATEflexflowDvr driver.
    /// </summary>
    public partial class ActivATEflexflowCtrl : Form, IControl
    {
        #region Private Members

        // Add an object variable that we can use to reference our driver class
        ActivATEflexflowDvr _myDvr;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ActivATEflexflowCtrl()
        {
            InitializeComponent();
        }

        #endregion

        #region IControl Members

        /// <summary>
        /// Gets or sets the form's text.
        /// </summary>
        public string Caption
        {
            get { return this.Text; }
            set { this.Text = value; }
        }

        /// <summary>
        /// Guaranteed to be called by the framework. Put your initialization code here.
        /// </summary>
        /// <param name="obj">The driver object for this GUI.</param>
        /// <returns>0 for success, non-zero otherwise.</returns>
        public int OnConnection(object obj)
        {
            try
            {
                // Get a handle to the driver
                _myDvr = obj as ActivATEflexflowDvr;

                if (_myDvr != null)
                {
                    // Plug in any event handlers that we may need
                    _myDvr.DeviceEvent += new DeviceEventHandler(myDvr_DeviceEvent);
                }
                else
                {
                    // Something went wrong...
                    labelStatus.Text = "ERROR: Driver reference is null.";
                }

                // Show the form
                this.Show();
            }
            catch (Exception)
            {
                // ToDo: error recovery/reporting here...
            }

            return 0;
        }

        /// <summary>
        /// Not called by the framework at this time.....
        /// Dev Note: Use FormClosing event to perform any cleanup.
        /// </summary>
        /// <returns></returns>
        public int OnDisconnection()
        {
            return 0;
        }

        #endregion

        #region DeviceEvent Handler

        private delegate void MyDeviceEventHandler(object sender, DeviceEventArgs args);

        /// <summary>
        /// Device event handler for events being generated from our driver class.
        /// </summary>
        /// <param name="sender">The driver that sent the event.</param>
        /// <param name="args">Defines the type of event (args.Attribute) and the event data (args.Data).</param>
        public void myDvr_DeviceEvent(object sender, DeviceEventArgs args)
        {
            // Make sure we're on the main GUI thread before we try to update any controls.
            if (InvokeRequired)
            {
                BeginInvoke(new MyDeviceEventHandler(DeviceEvent), new object[] { sender, args });
                return;
            }
            else
                DeviceEvent(sender, args);
        }

        /// <summary>
        /// Processes DeviceEvents on the correct thread context.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="args">Object containing event information.</param>
        protected void DeviceEvent(object sender, DeviceEventArgs args)
        {
            // No need to process events if we are hidden or closing.
            if (this.Visible == false)
                return;

            switch (args.Attribute)
            {
                // If your driver will fire any custom events (specific to this driver), check for
                // them here.  These custom event types should be numbered starting at 20 or greater
                // to avoid conflicting with inherited event types.
                case (int)eAttributes.Status:
                    labelStatus.Text = args.Data.ToString();
                    break;
                default:
                    // process defaults
                    break;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Simple helper to check for null reference of the driver object and display a message if it is null.
        /// </summary>
        /// <returns>False if driver object is null, otherwise true.</returns>
        private bool DriverOk()
        {
            if (_myDvr == null)
            {
                labelStatus.Text = "No driver object available.";
                return false;
            }
            else
                return true;
        }

        #endregion

        #region Form Event Handlers

        private void buttonDoWork_Click(object sender, EventArgs e)
        {
            if (DriverOk())
                _myDvr.DoWork();
        }

        #endregion
        /*
        private void btInitFlexFlow_Click(object sender, EventArgs e)
        {
            String serial = tbSerial.Text;
            String stationName = tbStationName.Text;
            String serviceUrl = tbServiceUrl.Text;
            
            if (DriverOk())
            {
                _myDvr.SetParameters(stationName,serviceUrl, serial);
            }
         
            
        }
        */
    }
}