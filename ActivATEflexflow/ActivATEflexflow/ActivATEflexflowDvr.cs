//=====================================================================================
// <copyright file="ActivATEflexflowDvr.cs" company="EADS North America">   
//     Copyright 2008, EADS North America Test And Services. All rights reserved.   
// </copyright>
//=====================================================================================
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// 
// THIS SOFTWARE MAY ONLY BE USED SUBJECT TO AN EXECUTED SOFTWARE LICENSE AGREEMENT 
// BETWEEN THE USER AND EADS.  YOU HAVE NO RIGHT TO USE OR EXPLOIT THIS MATERIAL 
// EXCEPT SUBJECT TO THE TERMS OF SUCH AN AGREEMENT.
//=====================================================================================

/**************************************************************************************
* Assembly Name:        ActivATEflexflow.dll
* 
* File Name:            ActivATEflexflowDvr.cs
* 
* Purpose:
* 
* Revision History:
* 
* Dev Notes:
* 
**************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Racal.Interfaces;
using FlexFlowClient;
using FlexFlowClient.fileutils;

namespace ActivATE.ActivATEflexflow
{
    /// <summary>
    /// Main class for the ActivATEflexflow driver.
    /// This class is accessed directly from the ActivATEflexflow test programs.
    /// </summary>
    [Serializable]
    public class ActivATEflexflowDvr : GenericDriver
    {
        //public object ReadFile { get; private set; }
        #region ActivATEflexflowDvr Defined Public APIs

        /// <summary>
        /// Initial Setup
        /// </summary>
        /// <param name="serviceURL"></param>
        /// <param name="userID"></param>
        /// <param name="stationName"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        public Boolean InitialSetup(string serviceURL, string userID, string stationName, out string strError)
        {
            bool isSuccessful = FFClientManager.Instance.InitialSetup(serviceURL, userID, stationName, out strError);
            FireEvent(new DeviceEventArgs((int)eAttributes.Status, strError));
            return isSuccessful;
        }

        

        private bool CheckParam(string paramName)
        {
            bool isSuccessful;
            if (paramName != "")
            {
                isSuccessful = true;
               
            }
            else
            {
                isSuccessful = false;
                FireEvent(new DeviceEventArgs((int)eAttributes.Status, String.Format("{0} cannot be empty", paramName)));
            }

            return isSuccessful;
        }

        public Boolean InitialSetup()
        {
            string serviceURL="";
            string userID="";
            string stationName="";
            string strError = "";
            bool isScuessful = FFClientManager.Instance.InitialSetup(serviceURL, userID, stationName, out strError);
            FireEvent(new DeviceEventArgs((int)eAttributes.Status, strError));
            return isScuessful;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serial"></param>
        /// <param name="strError"></param>
        /// <returns></returns>
        public Boolean QueryBoard(String serial, out String strError, ref string unitInfo)
        {
            bool isScuessful = FFClientManager.Instance.QueryBoard(serial, out strError, ref unitInfo);
            return isScuessful;
        }

        public Boolean SendTestResult(string testResult, string fileLoaction, out String strError)
        {
            Boolean isSucessful = false;
            String fileContent = "";
            isSucessful = ReadFile.ExtractXmlDataFromFile(fileLoaction, out fileContent,out strError);
            if (testResult.Equals("Passed"))
            {
                isSucessful=FFClientManager.Instance.RunBasicPassTest(out strError);
            }
            else
            {
                isSucessful = FFClientManager.Instance.RunBasicFailedTest(out strError);
            }
            return isSucessful;
        }

        public Boolean SendTestResultWithFile(string testResult, string fileLoaction, out String strError, out String xmlTestResult)
        {
            Boolean isSucessful = false;
            xmlTestResult = "";
            List<String> fileContent = new List<string>();
            String expression = "NODE_RESULT";
            isSucessful=ReadFile.ExtractXmlDataFromFile(fileLoaction, expression, out fileContent, out strError);
            if(isSucessful)
             isSucessful = FFClientManager.Instance.RunATETest(out strError, fileContent, testResult, out xmlTestResult); 
            return isSucessful;
        }

       

        /// <summary>
        /// Sample API: Can be called from the test program or from the GUI.
        /// </summary>
        /// <returns>Returns zero (0) if no error, non-zero otherwise</returns>
        public int DoWork()
        {
            int errorCode = 0;
            //

            // Fire an event to the GUI.  You may implement your own custom event types.
            // The GUI should check DeviceEventArgs.Attribute to determine the event type,
            // and then use the data in DeviceEventArgs.Data to process the event.
            // Custom event types should be numbered starting at 20 to avoid conflicting with
            // inherited event types.  Any object can be passed in DeviceEventArgs.Data,
            // including your own defined data structure.
            FireEvent(new DeviceEventArgs((int)eAttributes.Status, "DoWork called."));

            return errorCode;
        }

        #endregion      // User-Defined public API's

        #region IDevice Overrides

        /// <summary>
        /// Guaranteed to be called when instantiated. NOTE: Must override.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override int OnConnection(object obj)
        {
            return base.OnConnection(obj);
        }

        #endregion
    }
}
