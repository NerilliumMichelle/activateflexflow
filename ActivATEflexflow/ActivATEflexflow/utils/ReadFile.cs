﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ActivATE.ActivATEflexflow.utils
{
    public static class ReadFile
    {
        public static Boolean ExtractXmlDataFromFile(string pathToFile, 
            out string fileContent,
            out string  strError)
        {
            Boolean isSuccess = false;
            strError = "invalid";
            fileContent = "";
            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(pathToFile))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    fileContent = line;
                    isSuccess = true;


                }
            }
            catch (Exception e)
            {
                strError=String.Format("The file could not be read: {0}", e.Message);
              
            }
            return isSuccess;
        }

        public static Boolean ExtractXmlDataFromFile(string pathToFile, string expression,
            out List<string> fileContent,
            out string strError)
        {
            Boolean isSuccess = false;
            strError = "invalid";
            fileContent = new List<string>();
        
            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(pathToFile))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine().ToString();
                        if (line.Contains(expression))
                        {
                            fileContent.Add(line);
                        }
                    }
                    isSuccess = true;
                }
            }
            catch (Exception e)
            {
                strError = String.Format("The file could not be read: {0}", e.Message);

            }
            return isSuccess;
        }

        public static Boolean ExtractXmlDataFromFile1(string pathToFile, string expression,
            out List<string> fileContent,
            out string strError)
        {
            Boolean isSuccess = false;
            strError = "invalid";
            fileContent = null;
            try
            {
                fileContent = System.IO.File
                    .ReadAllLines(pathToFile)
                    .Where(i => i.Contains(expression))
                    .ToList();
                isSuccess = true;
            }
            catch (Exception e)
            {
                strError = String.Format("The file could not be read: {0}", e.Message);

            }
            return isSuccess;
        }
    }


}
